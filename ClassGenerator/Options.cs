﻿using CommandLine;

namespace ClassGenerator;

public class Options
{
    [Value(0, Required = true, MetaName = "layout-file", HelpText = "Input layout file")]
    public string InputFile { get; set; } = default!;
    
    [Option('o', "output", Required = false, HelpText = "Output file")]
    public string? OutputFile { get; set; }
    
    [Option('e', "extends", Default = null, HelpText = "Extension class")]
    public string? ExtendsClass { get; set; }
    
    [Option('n', "class-name", Default = null, HelpText = "Class name")]
    public string? ClassName { get; set; }
    
    [Option('s', "sub-widget", Default = null, HelpText = "Sub widget name")]
    public string? SubWidgetName { get; set; }

    [Option("skip-sub-widget-decl", Default = false, HelpText = "Don't declare sub-widget variable (useful with -e)")]
    public bool DontDeclareSubWidget { get; set; }
}
