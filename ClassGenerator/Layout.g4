grammar Layout;
@header {#pragma warning disable 3021}

layout
   : obj EOF
   ;

obj
   : Type ObjectName '{' pair* '}'
   ;

pair
   : variable
   | objList
   ;

objList
    : '{' obj* '}'
    ;
    
variable
    : ObjectName (StringLiteral | Number | Ident)*
    ;

Type
    : BasicType
    | GameType
    | HelperType
    | SpecialType
    ;

ObjectName
    : Ident
    | StringLiteral
    ;

Ident
    : Nondigit
    (
        Nondigit
        | Digit
    )*
    ;

StringLiteral
    :   '"' SCharSequence? '"'
    ;
    
Number
   : '-'? INT ('.' [0-9] +)? EXP?
   ;
   
   
fragment BasicType
    : 'ButtonWidgetClass'
    | 'CheckBoxWidgetClass'
    | 'ContentWidgetClass'
    | 'EditBoxWidgetClass'
    | 'EmbededWidgetClass'
    | 'FrameWidgetClass'
    | 'ImageWidgetClass'
    | 'MultilineEditBoxWidgetClass'
    | 'MultilineTextWidgetClass'
    | 'PanelWidgetClass'
    | 'ProgressBarWidgetClass'
    | 'RichTextWidgetClass'
    | 'SimpleProgressBarWidgetClass'
    | 'SliderWidgetClass'
    | 'TextListboxWidgetClass'
    | 'TextWidgetClass'
    | 'WindowWidgetClass'
    | 'XComboBoxWidgetClass'
    ;

fragment GameType
    : 'HtmlWidgetClass'
    | 'ItemPreviewWidgetClass'
    | 'MapWidgetClass'
    | 'PlayerPreviewWidgetClass'
    | 'ServerBrowserWidgetClass'
    | 'SmartPanelWidgetClass'
    | 'ThreeStateCheckboxWidgetClass'
    ;

fragment HelperType
    : 'GridSpacerWidgetClass'
    | 'ScrollWidgetClass'
    | 'WrapSpacerWidgetClass'
    ;

fragment SpecialType
    : 'CanvasWidgetClass'
    | 'GenericListboxWidgetClass'
    | 'RenderTargetWidgetClass'
    | 'RTTextureWidgetClass'
    | 'UniversalListboxWidgetClass'
    | 'VideoWidget'
    ;

fragment SCharSequence
    :   SChar+
    ;
    
fragment SChar
    :   ~["\\\r\n]
    |   EscapeSequence
    |   '\\\n'   // Added line
    |   '\\\r\n' // Added line
    ;

fragment EscapeSequence
    :   SimpleEscapeSequence
    ;

fragment SimpleEscapeSequence
    :   '\\' ['"?abfnrtv\\]
    ;

fragment Nondigit
    : [a-zA-Z_]
    ;

fragment Digit
    : [0-9]
    ;

fragment INT
   : '0' | [1-9] [0-9]*
   ;

// no leading zeros

fragment EXP
   : [Ee] [+\-]? INT
   ;

Whitespace
    : [ \t]+ -> skip
    ;

Newline
    :   (   '\r' '\n'?
        |   '\n'
        )
        -> skip
    ;
