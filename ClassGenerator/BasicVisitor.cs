﻿using System.Globalization;

public class BasicVisitor : LayoutBaseVisitor<object>
{
    public readonly List<Entity> Entities = new();

    public override object VisitPair(LayoutParser.PairContext context)
    {
        var variable = context.variable();

        if (variable == null) return base.VisitPair(context);

        var entity = Entities.Last();
        var varName = variable.ObjectName().GetText();

        entity.Variables ??= new Dictionary<string, object>();

        object? parsedList = null;
        if (variable.Number().Length != 0)
        {
            parsedList = variable.Number().Select(node => float.Parse(node.GetText(), new NumberFormatInfo
            {
                NumberDecimalSeparator = "."
            })).ToArray();
        }
        else if (variable.Ident().Length != 0)
        {
            parsedList = variable.Ident().Select(node => node.GetText()).ToArray();
        }
        else if (variable.StringLiteral().Length != 0)
        {
            parsedList = variable.StringLiteral().Select(node => node.GetText()).ToArray();
        }

        if (parsedList != null)
        {
            entity.Variables.Add(varName, parsedList);
        }

        return base.VisitPair(context);
    }

    public override object VisitObj(LayoutParser.ObjContext context)
    {
        var tp = context.Type().GetText();
        tp = tp[..tp.IndexOf("Class", StringComparison.Ordinal)];
        switch (tp)
        {
            // those widgets is not declared in EnforceScript
            case "PanelWidget":
            case "FrameWidget":
            case "SmartPanelWidget":
            case "ContentWidget":
            case "EmbededWidget":
            case "WindowWidget":
                tp = "Widget";
                break;
        }

        Entities.Add(new Entity
        {
            Type = tp,
            Name = context.ObjectName().GetText()
        });

        return base.VisitObj(context);
    }
}
