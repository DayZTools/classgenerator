﻿using ClassGenerator;
using CommandLine;

Generator? generator = null;

var helpWriter = new StringWriter();
var parser = new CommandLine.Parser(with => with.HelpWriter = helpWriter);
parser.ParseArguments<Options>(args).WithParsed(RunOptions).WithNotParsed(errs => DisplayHelp(errs, helpWriter));

generator?.Generate();

return 0;

void RunOptions(Options opts)
{
    generator = new Generator(opts);
}

void DisplayHelp(IEnumerable<Error> errs, TextWriter helpWriter)
{
    if (errs.IsVersion() || errs.IsHelp())
        Console.WriteLine(helpWriter.ToString());
    else
        Console.Error.WriteLine(helpWriter.ToString());
}
