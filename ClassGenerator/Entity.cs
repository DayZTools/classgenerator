﻿public class Entity
{
    public string Type { get; init; } = null!;
    public string Name { get; init; } = null!;

    public bool IgnorePointer
    {
        get
        {
            if (Variables == null) return false;
            var v = Variables.TryGetValue("ignorepointer", out var o);

            if (v == false || o == null) return false;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            var oA = ((float[])o)[0] == 1;

            return oA;
        }
    }

    public Dictionary<string, object>? Variables { get; set; } 
}
