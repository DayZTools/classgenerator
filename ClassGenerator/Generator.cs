﻿using Antlr4.Runtime;
using EnforceGenerator.CodeGenerators;

namespace ClassGenerator;

public class Generator : IDisposable
{
    private readonly BasicVisitor _visitor;

    private readonly StreamWriter _output;
    private readonly FileStream? _fileStream;

    private readonly Options _options;
    private string _subWidgetName = "m_subWidget";
    private string _extClassName = "ScriptedWidgetEventHandler";

    private ClassNode _classNode = new ClassNode();

    public Generator(Options opts)
    {
        _options = opts;
        var file = File.ReadAllText(_options.InputFile);

        var inputStream = new AntlrInputStream(file);
        var lexer = new LayoutLexer(inputStream);
        var commonTokenStream = new CommonTokenStream(lexer);
        var parser = new LayoutParser(commonTokenStream);
        var layoutContext = parser.layout();
        _visitor = new BasicVisitor();

        _visitor.Visit(layoutContext);

        if (opts.OutputFile == null)
        {
            _output = new StreamWriter(Console.OpenStandardOutput());
        }
        else
        {
            _fileStream = File.Open(opts.OutputFile, FileMode.Truncate);
            _output = new StreamWriter(_fileStream);
        }

        _output.AutoFlush = true;

        if (_options.SubWidgetName != null)
        {
            _subWidgetName = _options.SubWidgetName;
        }

        if (_options.ExtendsClass != null)
        {
            _extClassName = _options.ExtendsClass;
        }

        GenerateHeader();
        InitDeclarationsList();
        GenerateInitFunction();
        GenerateOnClickFun();
        InitOnUpdate();
    }

    public void Generate()
    {
        _output.Write(_classNode);
    }

    private void GenerateHeader()
    {
        var cname = _options.ClassName;

        if (_options.ClassName == null)
        {
            cname = Path.GetFileName(_options.InputFile).Split('.')[0];
        }

        _classNode.Name = new IdentNode { Value = cname! };
        _classNode.ExtendedFrom = new IdentNode { Value = _extClassName };
    }

    private void InitDeclarationsList()
    {
        foreach (var entity in _visitor.Entities)
        {
            _classNode.Variables.Add(new VariableDeclarationNode
            {
                Modifier = "protected",
                Type = entity.Type,
                Name = "m_" + entity.Name
            });
        }

        if (!_options.DontDeclareSubWidget)
        {
            _classNode.Variables.Add(new VariableDeclarationNode
            {
                Modifier = "protected",
                Type = "Widget",
                Name = _subWidgetName
            });
        }
    }

    private void InitOnUpdate()
    {
        var updateFunList = new List<FunctionDefinitionNode>();
        var onUpdateFn = new FunctionDefinitionNode
        {
            Modifier = "override",
            Type = GenBasicTypes.Void,
            Name = "Update",
            Params =
            {
                new VariableDeclarationNode { Type = GenBasicTypes.Float, Name = "timeslice" }
            }
        };

        foreach (var entity in _visitor.Entities)
        {
            if (entity.IgnorePointer)
            {
                continue;
            }

            if (entity.Type != "EditBoxWidget")
            {
                continue;
            }

            var fnName = "On" + Capitalize(entity.Name) + "Update";
            var newValName = Lowercase(entity.Name) + "NewVal";
            var oldValName = "m_" + Capitalize(entity.Name) + "OldVal";
            var callMethod = "";

            if (entity.Type == "EditBoxWidget")
            {
                callMethod = "GetText";
            }

            _classNode.Variables.Add(new VariableDeclarationNode
            {
                Modifier = "private",
                Type = "string",
                Name = oldValName
            });

            onUpdateFn.Code.Add(new VariableDeclarationNode
            {
                Type = "auto",
                Name = newValName,
                DefaultValue = new CallNode
                {
                    Object = new IdentNode { Value = "m_" + entity.Name },
                    Name = new IdentNode { Value = callMethod }
                }
            });

            onUpdateFn.Code.Add(new IfNode
            {
                Expression = new NotEqualNode
                    { Left = new IdentNode { Value = oldValName }, Right = new IdentNode { Value = newValName } },
                Block =
                {
                    new CallNode
                    {
                        Name = new IdentNode { Value = fnName }, Args =
                        {
                            new IdentNode { Value = oldValName },
                            new IdentNode { Value = newValName }
                        }
                    },
                    new AssignmentNode
                        { Left = new IdentNode { Value = oldValName }, Right = new IdentNode { Value = newValName } }
                }
            });

            updateFunList.Add(new FunctionDefinitionNode
            {
                Name = fnName,
                Type = GenBasicTypes.Void,
                Params =
                {
                    new VariableDeclarationNode { Type = GenBasicTypes.String, Name = "oldValue" },
                    new VariableDeclarationNode { Type = GenBasicTypes.String, Name = "newValue" },
                }
            });
        }

        _classNode.FunDefinitions.Add(onUpdateFn);
        _classNode.FunDefinitions.AddRange(updateFunList);
    }

    private void GenerateInitFunction()
    {
        var code = new List<BaseNode>();
        var initDecl = new FunctionDefinitionNode()
        {
            Type = "void",
            Name = "Init",
            Code = code
        };

        code.Add(new AssignmentNode
        {
            Left = new IdentNode { Value = _subWidgetName },
            Right = new CallNode
            {
                Name = new IdentNode { Value = "CreateWidgets" },
                Args = { new StringNode { Value = PathHelper.GetPathWithoutRoot(_options.InputFile) } }
            }
        });
        code.Add(new CallNode
        {
            Object = new IdentNode { Value = _subWidgetName },
            Name = new IdentNode { Value = "SetHandler" },
            Args = { new IdentNode { Value = "this" } }
        });

        foreach (var entity in _visitor.Entities)
        {
            code.Add(new AssignmentNode
            {
                Left = new IdentNode { Value = "m_" + entity.Name },
                Right = new CallNode
                {
                    Object = new IdentNode { Value = entity.Type },
                    Name = new IdentNode { Value = "Cast" },
                    Args =
                    {
                        new CallNode
                        {
                            Object = new IdentNode { Value = _subWidgetName },
                            Name = new IdentNode { Value = "FindAnyWidget" },
                            Args = { new StringNode { Value = entity.Name } }
                        }
                    }
                }
            });
        }

        _classNode.FunDefinitions.Add(initDecl);
    }

    private void GenerateOnClickFun()
    {
        var genClickFnList = new List<FunctionDefinitionNode>();
        var onClickFn = new FunctionDefinitionNode
        {
            Modifier = "override",
            Type = "bool",
            Name = "OnClick",
            Params =
            {
                new VariableDeclarationNode { Type = "Widget", Name = "w" },
                new VariableDeclarationNode { Type = "int", Name = "x" },
                new VariableDeclarationNode { Type = "int", Name = "y" },
                new VariableDeclarationNode { Type = "int", Name = "button" },
            },
        };
        onClickFn.Code.Add(new CallNode
        {
            Object = new IdentNode { Value = "super" },
            Name = new IdentNode { Value = "OnClick" },
            Args =
            {
                new IdentNode { Value = "w" },
                new IdentNode { Value = "x" },
                new IdentNode { Value = "y" },
                new IdentNode { Value = "button" },
            }
        });

        var sw = new SwitchNode { Expression = new IdentNode { Value = "w" } };

        foreach (var entity in _visitor.Entities)
        {
            if (entity.IgnorePointer)
            {
                continue;
            }

            if (entity.Type != "ButtonWidget" && entity.Type != "CheckBoxWidget")
            {
                continue;
            }

            var caseName = "m_" + entity.Name;
            var fnName = entity.Name;
            fnName = Capitalize(fnName) + "Click";

            var c = new CaseNode
            {
                Expression = new IdentNode { Value = caseName }
            };

            var clickCall = new CallNode
            {
                Name = new IdentNode
                {
                    Value = fnName
                }
            };

            var clickFn = new FunctionDefinitionNode
            {
                Type = GenBasicTypes.Void,
                Name = fnName
            };

            if (entity.Type == "CheckBoxWidget")
            {
                clickCall.Args.Add(new CallNode
                {
                    Object = new IdentNode { Value = caseName },
                    Name = new IdentNode { Value = "IsChecked" }
                });
                clickFn.Params.Add(new VariableDeclarationNode
                {
                    Type = GenBasicTypes.Bool,
                    Name = "checked"
                });
            }

            c.Code.Add(clickCall);
            c.Code.Add(new BreakNode());

            sw.Cases.Add(c);

            // generate click function
            genClickFnList.Add(clickFn);
        }

        onClickFn.Code.Add(sw);

        onClickFn.Code.Add(new ReturnNode { ReturnValue = new IdentNode { Value = "false" } });

        _classNode.FunDefinitions.Add(onClickFn);
        _classNode.FunDefinitions.AddRange(genClickFnList);
    }

    public void Dispose()
    {
        _output.Dispose();
        _fileStream?.Dispose();
    }

    public string Capitalize(string str)
    {
        return char.ToUpper(str[0]) + str.Substring(1);
    }

    public string Lowercase(string str)
    {
        return char.ToLower(str[0]) + str.Substring(1);
    }
}
