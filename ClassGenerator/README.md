﻿ClassGenerator
==============

This tool is designed to generate EnforceScript code from GUI ``*.layout`` files

Usage example:
```
ClassGenerator.exe myLayout.layout --extends MyUIBaseClass --class-name MyLayout
```

Parameters:
```
ClassGenerator.exe <layout-file> options
  -o, --output              Output file
  -e, --extends             Extension class
  -n, --class-name          Class name
  -s, --sub-widget          Sub widget name
  --skip-sub-widget-decl    (Default: false) Don't declare sub-widget variable (useful with -e)
  --help                    Display this help screen.
  --version                 Display version information.
  layout-file (pos. 0)      Required. Input layout file
```
