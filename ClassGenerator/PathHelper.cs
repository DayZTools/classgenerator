﻿namespace ClassGenerator;


public static class PathHelper
{
    public static string GetPathWithoutRoot(string orgPath)
    {
        return !Path.IsPathRooted(orgPath) ? orgPath : GetPathWithoutRoot(new DirectoryInfo(orgPath));
    }

    public static string GetPathWithoutRoot(DirectoryInfo dirInfo)
    {
        if (dirInfo.Parent == null)
        {
            return string.Empty; //here recursion will stop
        }

        var parent = GetPathWithoutRoot(dirInfo.Parent); //Call same function (Recursion)
        if (string.IsNullOrEmpty(parent))
        {
            return dirInfo.Name; //here start write path without root("C:\")
        }
        else
        {
            return Path.Combine(parent, dirInfo.Name);
        }
    }
}
